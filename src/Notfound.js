import React from 'react';
import logo from './logo.svg';
import './App.css';

function Notfound() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <h1>NOT FOUND</h1>
        </p>
        
      </header>
    </div>
  );
}

export default Notfound;
