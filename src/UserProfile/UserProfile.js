import React from 'react';
import Avatar from './Avatar';
import UserName from './UserName';
import Bio from './Bio';

const UserProfile = () => (
    <div className="container m-5">
        <Avatar />
        <UserName name="rudiansyahpratama" />
        <Bio />
    </div>
   
);


export default UserProfile;