// Render Prop
import React from 'react';
import logo from '../logo.svg';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const Basic = () => (
  <div>
    <Formik
      initialValues={{ email: '', password: '' }}
      validate={values => {
        const errors = {};
        if (!values.email) {
          errors.email = 'Required';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Invalid email address';
        }
        if(!values.password){
          errors.password = 'required';
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <div className="App">
        <header className="App-header">
            <div class="container">
            <img src={logo} className="App-logo" alt="" />
            <h1 class="mb-3">Log In</h1>
        <Form>
        <div class="form-group container col-6">
        <label  htmlFor="firstName">Email</label>
          <Field type="email" class="form-control" name="email" />
          <ErrorMessage name="email" component="div" />
        </div>
        <div class="form-group container col-6">
        <label htmlFor="firstName">password</label>
          <Field type="password" class="form-control" name="password" />
          <ErrorMessage name="password" component="div" />
        </div>
          <button type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
        </div>
        </header>
        </div>
        
      )}
    </Formik>
  </div>
);

export default Basic;