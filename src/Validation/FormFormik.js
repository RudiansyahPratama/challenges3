import React from 'react';
import { useFormik } from 'formik';
import logo from '../logo.svg';

// A custom validation function. This must return an object
// which keys are symmetrical to our values/initialValues
const validate = values => {
  const errors = {};
  
  if (!values.name) {
    errors.name = 'Name Harus Diisi';
  } else if (values.name.length > 20) {
    errors.name = 'Name Harus Kurang Dari 20 Karakter';
  }
  else if (values.name.length < 4) {
    errors.name = 'Name Harus Lebih Dari 4 Karakter';
  }

  if (!values.email) {
    errors.email = 'Email Harus Diisi';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if(!values.password) {
      errors.password = 'Password Harus Diisi'; 
  }else if(values.password !== values.confirmpassword){
      errors.confirmpassword = "password Harus Sama";
  }
  return errors;
};

const SignupForm = () => {
  // Pass the useFormik() hook initial form values and a submit function that will
  // be called when the form is submitted
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      password: '',
      confirmpassword: ''
    },
    validate,
    onSubmit: values => {
      alert(JSON.stringify(values, null, 2));
    },
  });
  return (
    <div className="App">
    <header className="App-header">
        <div class="container">
        <img src={logo} className="App-logo" alt="" width="400px"/>
        <h1 class="mb-3">Register</h1>
    <form onSubmit={formik.handleSubmit}>
    <div class="form-group row">
      <label htmlFor="firstName">Name</label>
      <input
        id="name"
        name="name"
        type="text"
        class="form-control"
        onChange={formik.handleChange}
        value={formik.values.name}
      />
      {formik.errors.name ? <div>{formik.errors.name}</div> : null}
      </div>
      <div class="form-group row">
      <label htmlFor="email">Email Address</label>
      <input
        id="email"
        name="email"
        type="email"
        class="form-control"
        onChange={formik.handleChange}
        value={formik.values.email}
      />
      {formik.errors.email ? <div>{formik.errors.email}</div> : null}
      </div>
      <div class="form-group row">
      <label htmlFor="password" >Password</label>
      <input
        id="password"
        name="password"
        type="password"
        class="form-control"
        onChange={formik.handleChange}
        value={formik.values.password}
      />
      {formik.errors.password ? <div>{formik.errors.password}</div> : null}
      </div>

      <div class="form-group row">
      <label htmlFor="password">Confirm Password</label>
      <input
        id="confirmpassword"
        name="confirmpassword"
        type="password"
        class="form-control"
        onChange={formik.handleChange}
        value={formik.values.confirmpassword}
      />
      {formik.errors.confirmpassword ?<div>{formik.errors.confirmpassword}</div> : null}
      </div>
      <div class="col-md-15 mb-4">
      <button type="submit" class="btn btn-outline-success">Submit</button>
      </div>
    </form>
    </div>
    </header>
        </div>
  );
};

export default SignupForm